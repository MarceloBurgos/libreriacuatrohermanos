﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Domicilio
    {
        [Required]
        public int DomicilioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Calle { get; set; }
        [Required]
        [MaxLength(45)]
        public string Ciudad { get; set; }
        [Required]
        [MaxLength(45)]
        public string Provincia { get; set; }
        [Required]
        [MaxLength(45)]
        public string CodigoPostal { get; set; }
    }
}