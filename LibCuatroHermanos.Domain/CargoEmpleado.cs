﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibCuatroHermanos.Domain
{
    public class CargoEmpleado
    {
        [Required]
        public int CargoEmpleadoId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(45)]
        public string Descripcion { get; set; }
        [Required]
        public decimal SueldoBase { get; set; }
    }
}
