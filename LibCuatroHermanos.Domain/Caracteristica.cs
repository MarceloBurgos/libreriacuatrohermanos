﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibCuatroHermanos.Domain
{
    public class Caracteristica
    {
        [Required]
        public int CaracteristicaId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [MaxLength(45)]
        public string Descripcion { get; set; }

        public List<PermisoRolUsuario> Permisos { get; set; }
    }
}
