﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class DetalleLiquidacionSueldo
    {
        [Required]
        public int DetalleLiquidacionSueldoId { get; set; }
        [Required]
        public int ConceptoSueldoId { get; set; }
        [Required]
        public int LiquidacionSueldoId { get; set; }

        public ConceptoSueldo ConceptoSueldo { get; set; }
        public LiquidacionSueldo LiquidacionSueldo { get; set; }
    }
}