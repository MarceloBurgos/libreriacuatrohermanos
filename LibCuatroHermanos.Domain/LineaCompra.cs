﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class LineaCompra
    {
        [Required]
        public int LineaCompraId { get; set; }
        [Required]
        public int CompraId { get; set; }
        [Required]
        public int ProductoId { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public decimal Subtotal { get; set; }

        public Compra Compra { get; set; }
        public Producto Producto { get; set; }
    }
}