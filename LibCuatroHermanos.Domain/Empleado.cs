﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Empleado
    {
        public Empleado()
        {
            Usuarios = new List<Usuario>();
        }

        [Required]
        public int EmpleadoId { get; set; }
        [Required]
        public int CargoEmpleadoId { get; set; }
        public int DomicilioId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(45)]
        public string Apellido { get; set; }
        [Required]
        [MaxLength(45)]
        public string DNI { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set;}
        [Required]
        [MaxLength(45)]
        public string Telefono { get; set; }
        [Required]
        [MaxLength(45)]
        public string Correo { get; set; }
        [Required]
        [MaxLength(45)]
        public string Legajo { get; set; }
        [Required]
        public DateTime FechaIngreso { get; set; }

        public CargoEmpleado CargoEmpleado { get; set; }
        public Domicilio Domicilio { get; set; }
        public List<Usuario> Usuarios { get; set; }
    }
}