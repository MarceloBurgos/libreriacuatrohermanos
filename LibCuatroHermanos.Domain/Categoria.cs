﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Categoria
    {
        [Required]
        public int CategoriaId { get; set; }
        [MaxLength(45)]
        [Required]
        public string Nombre { get; set; }
    }
}
