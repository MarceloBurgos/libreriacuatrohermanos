﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class Producto
    {
        [Required]
        public int ProductoId { get; set; }
        [Required]
        public int CategoriaId { get; set; }
        [Required]
        [MaxLength(45)]
        public string Nombre { get; set; }
        [MaxLength(45)]
        public string Descripción { get; set; }
        [Required]
        public decimal Precio { get; set; }
        [Required]
        public int Stock { get; set; }

        public Categoria Categoria { get; set; }
    }
}