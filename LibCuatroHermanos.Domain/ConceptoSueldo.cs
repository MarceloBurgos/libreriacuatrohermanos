﻿using System.ComponentModel.DataAnnotations;

namespace LibCuatroHermanos.Domain
{
    public class ConceptoSueldo
    {
        [Required]
        public int ConceptoSueldoId { get; set; }
        [Required]
        [MaxLength(4)]
        public string Codigo { get; set; }
        [Required]
        [MaxLength(45)]
        public string Descripcion { get; set; }
        [Required]
        public decimal Porcentaje { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public decimal Unitario { get; set; }
    }
}